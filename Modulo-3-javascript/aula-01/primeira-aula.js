//console.log(nomeDaLet);

var nome = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorDaConst";
const nomeDaConst2 = {
    nome: "Marcos",
    idade: 29
};
Object.freeze(nomeDaConst2);
nomeDaConst2.nome = "Marcos Henrique";
//console.log(nomeDaConst2);
/*function somar(){

}*/
/*function somar(valor1, valor2 = 1){
    resultado = valor1 + valor2;
    console.log(resultado);
}
somar(3);
somar(3, 2);
*/

/*function ondeMora(cidade) {
    console.log(`Eu moro em ${cidade}, e sou feliz durante ${30 + 1} dias.`);
}

ondeMora("Canoas");
*/

/*function fruteira() {
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pessego"
                + "\n";

    let newTexto = `
                    Banana
                    Ameixa
                    Goiaba
                    Pessego
                    `;
    console.log(texto);
    console.log(newTexto);
}

fruteira();
*/
let eu = {
    nome : "Anderson",
    idade : 33,
    altura : 1.84
};

/*
function quemSou( pessoa ){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} e ${pessoa.altura} de altura.`);
}
quemSou(eu);
*/

/*
let funcaoSomaValores = function( a, b ){
    return a + b;
}

let add = funcaoSomaValores;
let resultado = add(3, 5);
console.log(resultado);
*/
/*
const { nome : n, idade : i } = eu;
//console.log( nome, idade );
console.log( n, i );
*/

const array = [1, 3, 4, 8];
const [n1, ,n3, n2, n4 = 18] = array;
//console.log(n1, n2, n3, n4);

function testarPessoa({ nome, idade, altura }){
    console.log(nome, idade, altura);
}

//testarPessoa(eu);

let a1 = 42;
let b1 = 15;

[a1, b1] = [b1, a1];

console.log(a1, b1);
