//Calcula perimetro e area de circunferencia

let circulo = {
    raio: 3,
    operacao: "A"
}

function calculaCirculo({ raio, operacao }) {
    let resultado = 0;
    switch (operacao) {
        case "P":
            resultado = 2 * raio * Math.PI;
        case "A":
            resultado = Math.PI * Math.pow(raio, 2)
    }
    console.log(resultado);
}

calculaCirculo(circulo);

//Calcula bissexto

function naoBissexto(ano) {
    return !(ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0);
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

/*
Crie uma função somarPares que recebe um array de números (não precisa fazer validação) e soma todos números nas posições pares do array, exemplo:
*/

function somarPares(lista) {
    let soma = 0;
    for (let i = 0; i < lista.length; i += 2) {
        soma += lista[i];
    }
    console.log(soma);
}

somarPares([1, 56, 4.34, 6, -2]); // 3.34

/*
Escreva uma função adicionar que permite somar dois números através de duas
chamadas diferentes (não necessariamente pra mesma função).
*/

function adicionar(primeiro) {
    return function adicionar(segundo) {
        return primeiro + segundo;
    }
}
console.log(adicionar(3)(4)) // 7
console.log(adicionar(5642)(8749)) // 14391

/*
Escreva uma função imprimirBRL que recebe um número flutuante (ex: 4.651)
 e retorne como saída uma string no seguinte formato (seguindo o exemplo): “R$ 4,66”
*/
/*
function imprimirBRL(numero) {
    let moeda = Math.ceil(numero.toFixed(3) * 100)/100;
    return moeda.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL'} );
}

console.log(imprimirBRL(2313477.0135))
console.log(imprimirBRL(4.651))
*/
imprimirBRL = numero => (Math.ceil(numero.toFixed(3) * 100)/100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL'} );
console.log(imprimirBRL(2313477.0135))
console.log(imprimirBRL(4.651))


