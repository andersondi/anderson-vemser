import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';

class App extends Component {
  constructor(props) {
    super(props);
    //this.sortear = this.sortear.bind( this )
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state;
    //this.listaEpisodios.marcarComoAssistido(episodio)
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  /* avaliado() {
    const { episodio } = this.state;
    const valor = Number(document.getElementById('inputDisplay').value);
    if (valor >= 1 && valor <= 5) {
      this.listaEpisodios.atribuirNota(episodio, valor);
      this.setState({
        episodio
      })
    }
  } */
  registrarNota(evt) {
    const { episodio } = this.state
    episodio.avaliar(evt.target.value)
    this.setState({
      episodio,
      deveExibirMensagem: true
    })
    setTimeout(() => {
      this.setState({
        deveExibirMensagem: false
      })
    }, 5000)
  }


  geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual é a sua nota para este episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={this.registrarNota.bind(this)}></input>
            </div>
          )
        }
      </div>
    )
  }

  render() {
    const { episodio, deveExibirMensagem } = this.state
    return (
      <div className="App" >
        {/*         {deveExibirMensagem ? <span>Registramos sua nota!</span> : false } */}
        <span className={`flash green ${deveExibirMensagem ? '' : 'invisible'}`}>Registramos sua nota!</span>
        <header className='App-header'>
          <h2>{episodio.nome}</h2>
          <img src={episodio.url} alt={episodio.nome}></img>
          <span>{episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vez(es)</span>
          <span>Temporada/ Episodio: {episodio.temporadaEpisodio}</span>
          <span>Nota: {episodio.nota}</span>
          <h4>Duração: {episodio.duracao}</h4>
          <span className="mensagemDeSucesso"></span>

          <div>
            <button onClick={this.sortear.bind(this)}>Próximo</button>
            <button onClick={this.assistido.bind(this)}>Já assisti</button>
          </div>

          {this.geraCampoDeNota()}
        </header>
      </div>
    );
  }
}

export default App;
