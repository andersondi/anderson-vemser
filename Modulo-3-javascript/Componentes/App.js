import React, { Component } from 'react';
import './App.css';
import Membros from './components/Membros';
//import CompA, {CompB} from './components/exemploComponenteBasico';class App extends Component {
  // eslint-disable-next-line
  constructor( props ){
    super( props );
  }  render(){
    return (
      <div className="App">
        <Membros nome="João" sobrenome="Silva"/>
        <Membros nome="Maria" sobrenome="Silva"/>
        <Membros nome="Pedro" sobrenome="Silva"/>
        {/* <CompA />
        <CompA />
        <CompB />
        <CompA />
        <CompA />
        <CompA /> */}
      </div>
    );
  }
}export default App;