Array.prototype.invalidas = function () {
  const invalidas = this.filter(serie => {
    const algumCampoInvalido = Object.values(serie).some(campo => campo === null || typeof campo === 'undefined')
    const anoEstreiaInvalido = serie.anoEstreia > new Date().getFullYear()
    return algumCampoInvalido || anoEstreiaInvalido
  })

  return `Séries Inválidas: ${invalidas.map(serie => serie.titulo).join(' - ')}`
}

//Ex 2
Array.prototype.filtrarPorAno = function (ano) {
  return this.filter(serie => {
    return serie.anoEstreia >= ano
  })
}

//Ex 3
Array.prototype.procurarPorNome = function (nome) {
  let encontrou = false;
  this.forEach(serie => {
    let achou = serie.elenco.find(n => nome === n)
    if (achou) {
      encontrou = true
    }
  })
  return encontrou
}

//Ex 4
Array.prototype.mediaDeEpisodios = function () {
  return parseFloat(this.map(serie => serie.numeroEpisodios).reduce((prev, cur) => prev + cur, 0) / this.length)
}

//Ex 5
Array.prototype.totalSalarios = function (indice) {
  function imprimirBRLOneLine(valor) {
    return parseFloat(valor.toFixed(2)).toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL'
    })
  }

  const salarioDirecao = 100000 * this[indice].diretor.length
  const salarioAtores = 40000 * this[indice].elenco.length
  return imprimirBRLOneLine(parseFloat(salarioAtores + salarioDirecao))
}

//EX 6
Array.prototype.queroGenero = function (genero) {
  return this.filter(serie => {
    return Boolean(serie.genero.find(g => g === genero))
  })
}

Array.prototype.queroTitulo = function (titulo) {
  let palavrasTitulo = titulo.split(' ')
  return this.filter(serie => {
    let palavrasSerie = serie.titulo.split(' ')
    return Boolean(palavrasSerie.find(palavra => palavra.includes(palavrasTitulo)))
  })
}

//Ex 7
Array.prototype.creditos = function (indice) {

  this[indice].elenco.sort((a, b) => {
    let ArrayA = a.split(' ')
    let ArrayB = b.split(' ')
    return ArrayA[ArrayA.length - 1].localeCompare(ArrayB[ArrayB.length - 1])
  })

  this[indice].diretor.sort((a, b) => {
    let ArrayA = a.split(' ')
    let ArrayB = b.split(' ')
    return ArrayA[ArrayA.length - 1].localeCompare(ArrayB[ArrayB.length - 1])
  })

  let arrayParaJuntar = []

  arrayParaJuntar.push(`${this[indice].titulo}`)
  arrayParaJuntar.push(`Diretores`)

  this[indice].diretor.forEach(d => {
    arrayParaJuntar.push(`${d}`)
  })

  arrayParaJuntar.push(`Elenco`)

  this[indice].elenco.forEach(e => {
    arrayParaJuntar.push(`${e}`)
  })

  return arrayParaJuntar;
}

Array.prototype.procuraAbreviacao = function () {
  this.forEach(serie => {
    let palavraSecreta = [];
    serie.elenco.forEach(elem => {
      indice = (elem.split("").indexOf(".")) - 1;
      (indice > -1) ? palavraSecreta.push(elem[indice]) : "";
      if (serie.elenco.length == palavraSecreta.length) {
        return console.log(`#${palavraSecreta.join("")}`)
      }
    })
  })
}


console.log(series.procuraAbreviacao())
console.log(series.invalidas())
console.log(series.filtrarPorAno(2017))
console.log(series.procurarPorNome("Bernardo"))
console.log(series.procurarPorNome("Peter Dinklage"))
console.log(series.mediaDeEpisodios())
console.log(series.queroTitulo("Narcos"))
console.log(series.queroGenero("Caos"))
console.log(series.totalSalarios(8))
console.log(series.creditos(8))
