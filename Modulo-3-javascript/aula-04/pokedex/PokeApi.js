/* eslint-disable no-empty-function */
class PokeApi { // eslint-disable-line no-unused-vars
  // eslint-disable-next-line no-useless-constructor
  constructor() { }

  buscarTodos() {
    const fazRequisicao = fetch( 'https://pokeapi.co/api/v2/pokemon' );
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
  }

  buscar( id ) {
    const fazRequisicao = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` );
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
  }
}
