import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './Home';
import ListaAvaliacoes from './models/ListaAvaliacoes'

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/teste" component={ PaginaTeste } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path="/episodio/:id" component={ ListaAvaliacoes } />
      </Router>
    );
  }
}

const PaginaTeste = () =>
  <div>
    Pagina Teste
    <Link to="/">Home</Link>
    <Link to="/teste">Teste</Link>
  </div>