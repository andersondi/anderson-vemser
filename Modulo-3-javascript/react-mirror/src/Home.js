import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import { Link } from 'react-router-dom'
import EpisodioUi from './components/episodioUi';
import MensagemFlash from './components/MensagemFlash';
import MeuInputNumero from './components/MeuInputNumero';
//import Card from './components/card';

class Home extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      deveExibirErro: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota( { nota, erro } ){
    this.setState({
      deveExibirErro: erro
    })
    if (erro){
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    }else{
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }
    
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ( { cor, mensagem } ) => {
    this.setState( {
      cor,
      mensagem,
      exibirMensagem: true
    } )
  }

  /* geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) } ></input>
            </div>
          )
        }
      </div>
    )
  } */

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this
    return (
      <div className="App">
        { /*  deveExibirMensagem ? ( <span>Registramos sua nota!</span> ) : ''  */ }
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ exibirMensagem } 
                        mensagem={ mensagem }
                        cor={ cor }
                        segundos={ 5 } />
         <header className='App-header'>
          <EpisodioUi episodio={ episodio } />
          <div className="buttons">
            <button className="btn green" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn blue" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
            <button className="btn red">
              {/* <Link to="./avaliacoes"></Link> */}
              <Link to={ { pathname:"/avaliacoes", state: listaEpisodios } }>Avaliacao</Link>
            </button>
          </div>
          { /* this.geraCampoDeNota() */ }
          <MeuInputNumero placeholder="1 a 5"
                          mensagemCampo="Qual sua nota para esse episódio?"
                          visivel={ episodio.assistido || false }
                          obrigatorio={ true }
                          atualizarValor={ this.registrarNota.bind( this ) }
                          deveExibirErro={ deveExibirErro }
          />
        </header>

        {/* <Card>
          <EpisodioUi episodio={ episodio } />
          
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed neque non ex ullamcorper convallis. Vestibulum in malesuada nibh. Cras sed efficitur orci. Aliquam maximus mauris eget nibh sollicitudin bibendum. Nullam aliquam orci felis, et facilisis tellus aliquam id. Etiam porta euismod feugiat. Pellentesque consectetur porta ipsum, quis pharetra erat ullamcorper ut.</p>

          Mauris interdum rhoncus tortor. Aliquam euismod diam gravida, condimentum tellus vitae, euismod ipsum. Vestibulum vel ultrices quam. Vestibulum in est sapien. Ut interdum neque egestas urna aliquet eleifend. Etiam elementum sodales augue, luctus blandit nisi varius volutpat. Pellentesque ut metus aliquam, gravida lectus in, luctus erat. Duis aliquam pulvinar ipsum, semper placerat odio accumsan eget. Donec cursus venenatis diam quis commodo. Nullam quis leo dignissim, semper metus vel, vehicula magna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras molestie facilisis lectus at hendrerit.

          Sed volutpat suscipit pulvinar. Proin sit amet venenatis velit, ut rhoncus augue. Cras condimentum lacus et elementum aliquam. Phasellus quis faucibus nulla. Etiam quis aliquet sapien, sit amet accumsan sem. Praesent iaculis faucibus nunc, vel consequat sapien pharetra in. Aliquam erat volutpat. Donec dui lacus, interdum sit amet fringilla at, dignissim a dolor. Aenean nec molestie mauris. Donec convallis quam elit. Quisque ultrices eros justo, fermentum faucibus purus pretium accumsan. Maecenas blandit aliquet vehicula.

          In hac habitasse platea dictumst. Fusce a vestibulum lorem, vitae accumsan neque. Mauris ut porttitor ante, sit amet condimentum mi. Aenean blandit, est quis ornare vehicula, augue lorem egestas dolor, quis aliquam ante metus vel nisl. Morbi posuere vulputate faucibus. Aliquam eu massa velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin tempor ipsum vehicula urna vestibulum gravida. Suspendisse non libero aliquet, vestibulum est sit amet, cursus libero. Curabitur ac ligula ut erat gravida facilisis. Vivamus placerat massa nec urna dapibus dapibus. Sed nec ligula id dolor euismod faucibus.

          Integer vel faucibus nisl. Proin luctus condimentum quam, eget tincidunt nulla suscipit sed. Phasellus aliquam ut lorem eget fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed finibus ipsum non diam aliquam, a convallis velit suscipit. Morbi bibendum risus finibus pulvinar dapibus. Vivamus eu dictum dolor.
          <EpisodioUi episodio={ episodio } />
        </Card>
         */}
      </div>
    );
  }
}

export default Home;
