import React, { Component } from "react";
import { Link } from "react-router-dom";

import Logo from "../../assets/logo.svg";

import { Form, Container } from "./styles";

class Home extends Component {
  render() {
    return (
      <Container>
        <Form>
          <img src={Logo} alt="Logo do VemSer Bank" />
          <Link to ={{ pathname: "/listagem" }}><button type="submit">Clientes</button></Link>
          <Link to ={{ pathname: "/agencias" }}><button type="submit">Agências</button></Link>
          <Link to ={{ pathname: "/tiposDeConta" }}><button type="submit">Tipos de conta</button></Link>
          <Link to ={{ pathname: "/conta" }}><button type="submit">Conta</button></Link>
          <hr />
        </Form>
      </Container>
    );
  }
}

export default Home;