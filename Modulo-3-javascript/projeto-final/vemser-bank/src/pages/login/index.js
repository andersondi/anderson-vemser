import React, { Component } from "react";
import { Link } from "react-router-dom";
import ApiData from '../../services/api'

import Logo from "../../assets/logo.svg";

import { Form, Container } from "./styles";

class LoginPage extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    error: ""
  };

  render() {
    return (
      <Container>
        <ApiData />
        <Form>
          <img src={Logo} alt="Logo do VemSer Bank" />
          
          <input
            type="email"
            placeholder="Endereço de e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <input
            type="password"
            placeholder="Senha"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <hr />
          <Link to="/">Fazer login</Link>
        </Form>
      </Container>
    );
  }
}

export default LoginPage;