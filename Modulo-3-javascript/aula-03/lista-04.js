/*
Criar uma função que receba um multiplicador
 e vários valores a serem multiplicados, o retorno
  deve ser um array!
 */

multiplicar = (operador, ...numeros) => {
    let resultado = [];
    numeros.forEach(numero => resultado.push(operador * numero))
    return resultado;
}

//console.log(multiplicar(5, 3, 4))// [15, 20]
//console.log(multiplicar(5, 3, 4, 5))// [15, 20, 25]


/*
Criar um formulário de contato no site de vocês 
(Nome, email, telefone, assunto), ao sair do input
 de nome tem que garantir que tenha no minimo 10
 caracteres, garantir que em email tenha @ e ao
 clicar no botão de enviar tem que garantir que
 não tenha campos em branco.
*/

function validacao(e){
    let target = e.target;
    let msg = '';
    switch (target.name) {
        case "nome":
            if(target.value == ''){
                msg = "Esse campo não pode ser vazio";
            }else{
                msg = '';
            }  
            break;
        case "email":
            break;
        default:
            break;
    }
    let erro = document.getElementById(`msg-erro-${target.name}`)
    erro.innerHTML = msg;
}

let vazio = document.getElementsByClassName('vazio');
for (let i = 0; i < vazio.length; i++) {
    vazio[i].addEventListener('blur', (e) => validacao(e));
}
