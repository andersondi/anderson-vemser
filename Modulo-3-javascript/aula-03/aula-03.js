function criarSanduiche( pao, recheio, queijo, salada ){
    console.log(`
    Seu Sanduiche tem o pão ${pao}
    com recheio de ${recheio}
    e queijo ${queijo} com 
    salada se ${salada}`);
}

const ingredientes = ['3 queijos', 'Frango', 'Tomate e Alface'];

//criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

//console.log([..."Anderson"]);


let inputTeste = document.getElementById('campoTeste');

inputTeste.addEventListener('blur', () => {
    alert("Obrigado!")
})
