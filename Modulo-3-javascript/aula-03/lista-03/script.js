function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  var i = cardapio.length
  if (!comLactose) {
    cardapio.push('pastel de queijo')
  }

  cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa'];

  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)

    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1)

  }
  let resultado = cardapio
                          //.filter(alimento => alimento.length > 12)
                          .map(alimento => alimento.toUpperCase());

  return resultado
}
console.log(cardapioIFood()) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]