import React from 'react';
import Header from './components/header';
import './App.css';
import Footer from './components/footer';
import Button from './components/button';

function Home() {
  return (
    <div className="App">
      <Header />
      <section class="main-banner">
        <article>
          <h1>Vem ser DBC</h1>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.
            </p>
          <Button class="button button-big button-outline" name="Saiba mais" href="#" />
        </article>
      </section>
      <Footer />
    </div>
  );
}

export default Home;
