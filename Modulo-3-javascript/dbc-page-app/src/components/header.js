import React from "react";
import Menu from './menu';
import './css/header.css'
import './css/grid.css'
import './css/reset.css'

function Header() {
  return (
    <header class="main-header">
      <nav class="container clearfix">
        <a class="logo" href="index.html" title="Voltar à home">
          <img src="img/logo-dbc-topo.png" alt="DBC Company" />
        </a>

        <label class="mobile-menu" for="mobile-menu">
          <span></span>
          <span></span>
          <span></span>
        </label>
        <input id="mobile-menu" type="checkbox" />
        <Menu />
      </nav>
    </header>
  );
}

export default Header;



