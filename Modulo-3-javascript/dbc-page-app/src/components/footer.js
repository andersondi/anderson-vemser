import React from "react";
import Menu from './menu'

function Footer() {
  return (
    <footer class="main-footer">
      <div class="container">
        <Menu />
        <p>
          &copy; Copyright DBC Company - 2019
          </p>
      </div>
    </footer>
  );
}

export default Footer;