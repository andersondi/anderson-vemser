import React from 'react';
import './css/buttons.css'

function Button(props) {
  return (
    <React.Fragment>
      <a className={props.class} href={props.href}>{props.name}</a>
    </React.Fragment>

  );
}

export default Button;
