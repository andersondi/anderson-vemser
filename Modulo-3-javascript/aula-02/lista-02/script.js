//Calcula perimetro e area de circunferencia

let circulo = {
    raio: 3,
    operacao: "A"
}

function calculaCirculo({ raio, operacao }) {
    let resultado = 0;
    switch (operacao) {
        case "P":
            resultado = 2 * raio * Math.PI;
        case "A":
            resultado = Math.PI * Math.pow(raio, 2)
    }
    console.log(resultado);
}

calculaCirculo(circulo);

//Calcula bissexto

function naoBissexto(ano) {
    return !(ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0);
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

/*
Crie uma função somarPares que recebe um array de números (não precisa fazer validação) e soma todos números nas posições pares do array, exemplo:
*/

function somarPares(lista) {
    let soma = 0;
    for (let i = 0; i < lista.length; i += 2) {
        soma += lista[i];
    }
    console.log(soma);
}

somarPares([1, 56, 4.34, 6, -2]); // 3.34

/*
Escreva uma função adicionar que permite somar dois números através de duas
chamadas diferentes (não necessariamente pra mesma função).
*/

function adicionar(primeiro) {
    return function adicionar(segundo) {
        return primeiro + segundo;
    }
}
console.log(adicionar(3)(4)) // 7
console.log(adicionar(5642)(8749)) // 14391

/*
Escreva uma função imprimirBRL que recebe um número flutuante (ex: 4.651)
 e retorne como saída uma string no seguinte formato (seguindo o exemplo): “R$ 4,66”
*/
/*
function imprimirBRL(numero) {
    let moeda = Math.ceil(numero.toFixed(3) * 100)/100;
    return moeda.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL'} );
}
*/
/*
*/
/*
imprimirBRL = numero => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
imprimirGBP = numero => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('en-GB', { style: 'currency', currency: 'GBP' });
imprimirFR = numero => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' });

console.log(imprimirBRL(4.651))
console.log(imprimirGBP(4.651))
console.log(imprimirFR(4.651))
console.log(imprimirMoeda(2313477.0135, FR))
*/
/*
let moedas = ( function (){
    //Tudo é Privado
    function imprimirMoeda( params ) {
        function arredondar(numero, precisao = 2){
            const fator = Math.pow( 10, precisao );
            return Math.ceil( numero * fator ) / fator;
        }
    
        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params
    
        let qtdCasasMilhares = 3
        let StringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero)%1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length

        let c = 1
        while (parteInteiraString.length > 0) {
            if(c % qtdCasasMilhares == 0){
                StringBuffer.push( `${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
                parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
            }else if (parteInteiraString.length < qtdCasasMilhares){
                StringBuffer.push( parteInteiraString )
                parteInteiraString = ''
            }
            c++
        }
        StringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, 0)
        const numeroFormatado = `${ StringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }

    //Tudo é Publico
    return {
        imprimirBRL: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
            })
    }
})()

console.log(moedas.imprimirBRL(1000.00000));
*/
let moedas = (function ( tipoMoeda ) {
    switch (tipoMoeda) {
        case "BRL":
            imprimirBRL = (numero) => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        case "GBP":
            imprimirGBP = (numero) => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('en-GB', { style: 'currency', currency: 'GBP' });
        case "FR":
            imprimirFR = (numero) => (Math.ceil(numero.toFixed(3) * 100) / 100).toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' });
    }
})
console.log(moedas.imprimirBRL("BRL", 2313477.0135 ))
//console.log(imprimirMoeda(GBP, 2313477.0135 ))



