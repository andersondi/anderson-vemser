class Personagem{
    constructor( nome, experiencia = 0, vida = 10 ) {
        this.__nome = nome;
        this.__experiencia = experiencia;
        this.__vida = vida;
    }

    get nome(){
        return this.__nome;
    }

    get experiencia(){
        return this.__experiencia;
    }

    get vida(){
        return this.__vida;
    }

    set incrementaVida( valor ){
        this.__vida += valor;
    }

    set decrementaVida( valor ){
        this.__vida -= valor;
    }
}
const luke = new Personagem( 'Luke Skywalker' )

luke.incrementaVida = 10;
console.log( luke.vida )
