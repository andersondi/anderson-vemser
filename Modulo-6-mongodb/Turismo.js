/*Exercício 01
E devemos inserir pelo menos 6 cidades, com data de fundação, KM de Porto alegre e entre 2 e 6 (vão alternando)
 pontos turísticos dessa cidade com descrição (valor de entrada, horário de funcionamento, etc).
Exercício 02
Altere as cidades, onde uma deve ser inserido elementos, uma segunda retirado e uma terceira precisa ser mudado os valores.
Exercício 03
Delete uma das cidades (a que acham que vale menos a pena ir).
Exercício 04
Quero um filtro para pesquisar por valor de entrada e por distância.
*/
db.createCollection("Turismo")
db.Turismo.insert([
  {
  nome: "Osório",
  fundacao: new Date("1857-12-16"),
  distanciaAteACapitalKm: 95,
  pontosTuristicos: [
    {
      nome: "Mirante",
      descricao: "O mirante é uma plataforma de 50 metros quadrados, garantindo uma visão privilegiada da faixa litorânea, do parque eólico e do complexo das lagoas, além da parte posterior do morro.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Lagoa dos Barros",
      descricao: "A Lagoa dos Barros é um local excelente para banho. Suas águas não são poluídas, praias calmas, sem buracos. É a lagoa com a maior captação de água do planalto.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
},
{
  nome: "Gramado",
  fundacao: new Date("1954-12-15"),
  distanciaAteACapitalKm: 95,
  pontosTuristicos: [
    {
      nome: "Lago Negro",
      descricao: "O Lago Negro é um lago artificial e oferece passeio de pedalinhos, bar, restaurante e loja de conveniências.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Museu dos Festivais de Cinema",
      descricao: "A Lagoa dos Barros é um local excelente para banho. Suas águas não são poluídas, praias calmas, sem buracos. É a lagoa com a maior captação de água do planalto.",
      custoEntrada: 20.0,
      horarioFuncionamento: "11h às 21h"
    }
  ]
},
{
  nome: "Bento Gonçalves",
  fundacao: new Date("1890-08-11"),
  distanciaAteACapitalKm: 109,
  pontosTuristicos: [
    {
      nome: "Trem do Vinho",
      descricao: "O Trem do Vinho, também conhecido como Trem da Uva ou simplesmente Maria Fumaça, é uma linha ferroviária turística localizada no Vale dos Vinhedos.",
      custoEntrada: 185.12,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Vale dos Vinhedos",
      descricao: "O Vale dos Vinhedos, rota mais visitada no município, recebe anualmente cerca de 410 mil visitantes, sendo considerado o principal destino enoturístico do Brasil, oferecendo visitas nas vinícolas e outros estabelecimentos locais, além de deslumbrar a paisagem recheada de parreirais.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
},
{
  nome: "Caxias do Sul",
  fundacao: new Date("1890-06-20"),
  distanciaAteACapitalKm: 128,
  pontosTuristicos: [
    {
      nome: "Museu Ambiência Casa de Pedra",
      descricao: "O museu é dedicado à reconstituição do modo de vida doméstico dos inícios da colonização italiana na cidade.",
      custoEntrada: 0,
      horarioFuncionamento: "9h às 17h"
    },
    {
      nome: "Catedral de Caxias do Sul",
      descricao: "A Catedral é um dos mais importantes edifícios históricos da cidade e um dos mais distinguidos exemplares da arquitetura colonial na zona de imigração italiana da serra gaúcha.",
      custoEntrada: 0,
      horarioFuncionamento: "8h às 21h"
    }
  ]
},
{
  nome: "Torres",
  fundacao: new Date("1878-05-21"),
  distanciaAteACapitalKm: 208,
  pontosTuristicos: [
    {
      nome: "Praia da Cal",
      descricao: "Praia localizada entre o Morro do Farol e o Morro das Furnas, cujo nome se deve à antiga presença de fornos de torrefação de conchas retiradas de sambaquis para a fabricação de cal.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Ilha dos Lobos",
      descricao: "A única ilha do litoral riograndense, que sobressai apenas cerca de 2m do nível do mar e já foi responsável por vários naufrágios.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
},
{
  nome: "Tramandaí",
  fundacao: new Date("1965-09-24"),
  distanciaAteACapitalKm: 118,
  pontosTuristicos: [
    {
      nome: "Praia",
      descricao: "Aguá verde.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    },
    {
      nome: "Ponte do Rio Tramandaí",
      descricao: "Lugar onde os turistas tentam pescar.",
      custoEntrada: 0,
      horarioFuncionamento: "Sempre aberto"
    }
  ]
}
])

db.Turismo.updateOne( {nome: "Gramado"}, {$push: {pontosTuristicos: {nome: "Mini Mundo", descricao: "O Mini Mundo é um parque ao ar livre formado por réplicas fiéis de prédios de várias partes do mundo, ricas em detalhes e únicas, baseadas em seus respectivos projetos originais.", custoEntrada: 42.0, horarioFuncionamento: "9h às 17h"}}} )
db.Turismo.updateOne( {nome: "Gramado"}, {$set: {distanciaAteACapitalKm: 115}})
db.Turismo.update( { nome: "Gramado"}, {$pull: {"pontosTuristicos" : { nome : "Lago Negro" } }})
db.Turismo.remove( { nome: "Tramandaí"} )

db.Turismo.find({ "pontosTuristicos.custoEntrada": { $gte : 20.00 }, "distanciaAteACapitalKm" : 95 } )

