import React from 'react';
// import logo from './logo.svg';
import './App.css';
import EventCalendar from './components/EventCalendar/EventCalendar.js';

function App() {
  return (
    <EventCalendar/>
  );
}

export default App;
