const fs = require('fs');

// const makeDir = (...dirs) => {
//   dirs.forEach(dir => {
//     if (!fs.existsSync(dir)) {
//       fs.mkdirSync(dir);
//     }
//     console.log(`Diretorio ${dir} criado`);
//   }
// }

const save = (stringContent, contentFilePath = "./", fileName = "file", extension = ".txt") => {
  fs.writeFile(`${contentFilePath}${fileName}${extension}`, stringContent, (error) => {
    if (error) {
      throw error
    }
    console.log(`Arquivo ${fileName} criado`);
  });
}

module.exports = {
  // makeDir,
  save
}