/* Classe elfo */
public class Elfo extends Personagem{
    //DECLARA atributos

    private int indiceFlecha;
    private static int qtdElfos;
    //ATRIBUI os atributos
    {
        indiceFlecha = 0;
    }

    //CONSTRUTOR de objeto Elfo
    public Elfo( String nome ) {
        super( nome );
        this.vida = 100.0;
        this.inventario.adicionarItem( new Item(2, "Flecha") );
        this.inventario.adicionarItem( new Item(1, "Arco") );
        Elfo.qtdElfos++;
    }

    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }

    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }

    public Item getFlecha() {
        return this.inventario.obterPosicao( indiceFlecha );
    }

    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }

    public void dispararFlecha( Dwarf dwarf ) {
        //Dwarf dwarf = new Dwarf("Guimly");
        if ( this.getQtdFlecha() > 0 ){
            getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXp();
            dwarf.sofrerDano();
            this.sofrerDano();
        }
    }
}
