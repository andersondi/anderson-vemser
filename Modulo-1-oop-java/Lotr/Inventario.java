import java.util.*;

public class Inventario {

    private int tamanho;

    private ArrayList<Item> itens;

    //CONSTRUTOR do objeto Inventario com uma QUANTIDADE escolhida
    public Inventario( int quantidade ) {
        this.itens = new ArrayList<Item>( quantidade );
    }

    public ArrayList<Item> getItens(){
        return itens;
    }

    public void ordenarItens() {
        this.ordenarItens( TipoOrdenacao.ASC );
    }

    public void ordenarItens( TipoOrdenacao ordenacao ) {
        for ( int i = 0; i < this.itens.size(); i++ ) {
            for ( int j = 0; j < this.itens.size() - 1; j++ ) {
                Item atual = this.itens.get( j );
                Item proximo = this.itens.get( j + 1 );
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                        atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();

                if( deveTrocar ){
                    Item itemTrocado = atual;
                    this.itens.set( j, proximo );
                    this.itens.set( j + 1, itemTrocado );
                }
            }
        }
    }

    public String getNomesItens() {
        StringBuilder listaDeNomes = new StringBuilder();
        for( int i = 0; i < this.itens.size(); i++ ) {
            Item item = this.itens.get(i);
            if( item != null ) {
                listaDeNomes.append( item.getNome() );
                listaDeNomes.append(",");
            }
        }
        return (listaDeNomes.length() > 0 ?
                listaDeNomes.substring(0, ( listaDeNomes.length() - 1 ) ):
            listaDeNomes.toString() );
    }

    public Item getItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;

        for( int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get( i );
            if( item != null ){
                if( item.getQuantidade() > maiorQuantidade ) {
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }
            }
        }

        return this.itens.size() > 0 ? this.itens.get( indice ) : null;
    }

    public Item buscar( String descricao ){
        for ( Item itemAtual : this.itens ) {
            boolean encontrei = itemAtual.getNome().equals(descricao);

            if( encontrei ){
                return itemAtual;
            }
        }
        return null;
    }

    public ArrayList<Item> inverter() {
        ArrayList<Item> inverso = new ArrayList<>(this.itens.size());
        for ( int i = this.itens.size() -1 ; i >= 0; i-- ) {
            inverso.add(this.itens.get(i));
        }
        return inverso;
    }

    public int tamanhoDoInventario() {
        return this.getItens().size();
    }

    public void adicionarItem( Item item ) {
        this.itens.add( item );
    }

    public Item obterPosicao( int posicao ) {
        if( posicao >= this.itens.size() ) {
            return null;
        }
        return this.itens.get( posicao );
    }

    public void removerItem( Item item ) {
        this.itens.remove(item);
    }

    public Inventario unir( Inventario novoInventario ) {
        Inventario inventarioUnido = this;
        return inventarioUnido;
    }
}