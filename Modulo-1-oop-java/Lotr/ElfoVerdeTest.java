
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void elfoVerdeGanha2XpPorFlecha() {
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        elfoVerde.dispararFlecha(new Dwarf("Balin"));
        assertEquals(2, elfoVerde.getExperiencia());
    }
}
