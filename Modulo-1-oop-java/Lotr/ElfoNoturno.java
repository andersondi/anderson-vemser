
public class ElfoNoturno extends Elfo {
    public ElfoNoturno( String nome ) {
        super( nome );
        this.qtdExperienciaPorAtaque = 3;
        this.danoSofrido = 15.0;
    }
}
