
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    
    @After
    public void Teardown(){
        System.gc();
    }

    @Test
    public void atirar1FlechaDiminuirFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Guimly");
        novoElfo.dispararFlecha(novoAnao);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
    }

    @Test
    public void atirar2FlechaDiminuirFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Guimly");
        for (int i = 0; i < 2; i++){ 
            novoElfo.dispararFlecha(novoAnao);
        }
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }

    @Test
    public void atirar3FlechaDiminuirFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Guimly");
        for (int i = 0; i < 3; i++){ 
            novoElfo.dispararFlecha(novoAnao);
        }
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }

    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }

    @Test
    public void naoCriaElfoNaoIncrementa(){
        assertEquals( 0, Elfo.getQtdElfos() );
    }

    @Test
    public void Cria1ElfoContadorUmaVez(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals( 1, Elfo.getQtdElfos() );
    }

    @Test
    public void Cria2ElfoContadorDuasVez(){
        Elfo novoElfo1 = new Elfo("Legolas");
        Elfo novoElfo2 = new Elfo("Elrond");

        assertEquals( 2, Elfo.getQtdElfos() );
    }
}
