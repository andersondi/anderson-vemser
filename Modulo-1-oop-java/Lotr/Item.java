
public class Item {
    
    protected int quantidade;
    private String nome;
    

    //CONSTRUTOR do objeto Item
    public Item( int quantidade, String nome ) {
        this.quantidade = quantidade;
        this.nome = nome;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade( int quantidade ) {
        this.quantidade = quantidade;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public boolean equals( Object obj ) {
        Item outroItem = (Item)obj;
        return this.quantidade == outroItem.getQuantidade() && this.nome.equals(outroItem.getNome());
    }
    
    
    
    
}
