
public class ExercitoQueAtaca{

    private Estrategias estrategia;

    public ExercitoQueAtaca ( Estrategias estrategia ){
        this.estrategia = estrategia;
    }

    public void trocarEstrategia( Estrategias estrategia ){
        this.estrategia = estrategia;
    }

}
