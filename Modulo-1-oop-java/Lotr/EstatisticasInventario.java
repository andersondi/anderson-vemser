
public class EstatisticasInventario {
    private Inventario inventario;

    //CONSTRUTOR do objeto EstatisticasInventario
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
    //retorna a media de quantidades de itens do inventario
    public double calcularMedia() {
        if (this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        double buffer = 0; //aqui sao somados os valores de quantidade um a um
        double media = 0;
        //isto e um for each (para cada)
        //varre cada item do tipo Item que esta em inventario.getItens e soma ao buffer
        for(Item item : this.inventario.getItens()) {
            buffer += item.getQuantidade();
        }
        media = buffer / inventario.getItens().size();//aqui o valor da soma eh dividido pela quantidade
        return media;
    }
    //retorna a mediana da quantidade de itens
    public double calcularMediana() {
        double mediana = 0;

        if( inventario.tamanhoDoInventario()%2 == 0 ) {
            double valorCentral1 = 0;
            double valorCentral2 = 0;

            valorCentral1 = inventario.getItens().get( ( inventario.tamanhoDoInventario() / 2 ) - 1 ).getQuantidade();
            valorCentral2 = inventario.getItens().get( inventario.tamanhoDoInventario() / 2 ).getQuantidade();

            mediana = (double)(( valorCentral1 + valorCentral2 ) / 2);
        }else {
            mediana = (double)(inventario.getItens().get( ( ( inventario.tamanhoDoInventario() + 1 ) / 2 ) - 1 ).getQuantidade());
        }
        return mediana;
    }
    //retorna a quantidade de itens que estão acima da média de quantidades
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int counter = 0;
        for( Item item : inventario.getItens() ) {
            if( item.getQuantidade() > media ) {
                counter++;
            }
        }

        return counter;
    }
}
