
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void atacarComEspada(){
        ElfoDaLuz novoElfoDaLuz = new ElfoDaLuz("Legolas");
        Dwarf novoAnao = new Dwarf("Guimly");
        novoElfoDaLuz.atacarComEspada(novoAnao);
        assertEquals(79.0, novoElfoDaLuz.getVida(), 1e-9);
        novoElfoDaLuz.dispararFlecha(novoAnao);
        assertEquals(89.0, novoElfoDaLuz.getVida(), 1e-9);
    }
}
