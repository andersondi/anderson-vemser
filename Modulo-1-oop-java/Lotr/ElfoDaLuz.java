import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo {

    boolean ataqueImpar = false;

    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
            Arrays.asList(
                "Espada de Galvorn"
            )
        );

    public ElfoDaLuz( String nome ) {
        super( nome );
        super.ganharItem( new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
        
        this.danoSofrido = 21.0;
    }

    @Override
    public void perderItem( Item item ) {
        boolean podePerder = !DESCRICOES_OBRIGATORIAS.contains(item.getNome());
        if( podePerder ) {
            super.perderItem( item );
        }
    }

    public void ganharVida() {
        this.vida = vida + 10.0;
    }

    public void atacarComEspada( Dwarf dwarf ) {
        if( this.ataqueImpar ) {
            this.ganharVida();
        }else{
            this.sofrerDano();
        }
        dwarf.sofrerDano();
        this.aumentarXp();
        this.ataqueImpar = !this.ataqueImpar;
    }

}
