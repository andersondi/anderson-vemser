import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {

    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.getItens().get(0));
        //assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        //assertEquals(espada, inventario.obter(0));
        assertEquals(escudo, inventario.getItens().get(1));
        //assertEquals(escudo, inventario.obter(1));
    }
    
    @Test
    public void obterItem() {
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem(espada);
        assertEquals(espada, inventario.obterPosicao(0));
    }
    
    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.removerItem(espada);
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item lanca = new Item(1, "Lança");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(lanca);
        inventario.adicionarItem(escudo);
        assertEquals("Espada,Lança,Escudo", inventario.getNomesItens());
    }
    
    @Test
    public void getDescricoesNenhumItem(){
        Inventario inventario = new Inventario(11);
        assertEquals("", inventario.getNomesItens());
    }
    
    @Test
    public void getItemMaiorQuantidade(){
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item lanca = new Item(5, "Lança");
        Item escudo = new Item(3, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(lanca);
        inventario.adicionarItem(escudo);
        assertEquals(lanca , inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeQuantidadesIguais(){
        Inventario inventario = new Inventario(11);
        Item espada = new Item(1, "Espada");
        Item lanca = new Item(1, "Lança");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(lanca);
        inventario.adicionarItem(escudo);
        assertEquals(espada , inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void buscarItemDescricao(){
        Inventario inventario = new Inventario(11);
        Item celular = new Item(1, "Celular");
        inventario.adicionarItem(celular);
        assertEquals( celular, inventario.buscar("Celular") );
    }
    
    @Test
    public void buscarItemDescricaoMesmaDescricao(){
        Inventario inventario = new Inventario(11);
        Item celular = new Item(1, "Celular");
        Item celular1 = new Item(1, "Celular");
        inventario.adicionarItem(celular);
        inventario.adicionarItem(celular1);
        assertEquals( celular, inventario.buscar("Celular") );
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(1);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void inverterInventarioUmItem(){
        Inventario inventario = new Inventario(1);
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(arco);
        assertEquals(arco, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    }
    
    @Test
    public void inverterInventarioDoisItens(){
        Inventario inventario = new Inventario(1);
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(1, "Flecha");
        inventario.adicionarItem(arco);
        inventario.adicionarItem(flecha);
        assertEquals(flecha, inventario.inverter().get(0));
        assertEquals(arco, inventario.inverter().get(1));
        assertEquals(2, inventario.inverter().size());
    }
}
