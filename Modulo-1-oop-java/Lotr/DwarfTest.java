
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }

    @Test
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }

    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for ( int i = 0; i < 11; i++ ){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }

    @Test
    public void dwarfPerdeTodaVida12Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for ( int i = 0; i < 12; i++ ){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }

    /*
     * Se Dwarf ficar com 0 de vida, entao status dele para MORTO.
     * Dwarf não pode perder vida depois de morto.
     */
    @Test
    public void dwarfStatusMortoQuandoFicaComZeroDeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for ( int i = 0; i < 11; i++ ){
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
    }

    @Test
    public void testaGetStatus(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
    @Test
    public void testaSetStatus(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.setStatus( Status.MORTO );
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void testaVerificaSeEstaVivo(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(true, dwarf.verificaSeEstaVivo());
    }
    
    @Test
    public void testaVerificaSeEstaMorto(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.setStatus( Status.MORTO );
        assertEquals(false, dwarf.verificaSeEstaVivo());
    }
}
