import java.util.*;

public class PaginadorInventario {
    private ArrayList<Item> itens;
    private int marcadorInicial = 0;

    private Inventario inventario;
    private PaginadorInventario paginadorInventario;

    public PaginadorInventario( Inventario inventario ) {
        this.inventario = inventario;
    }

    //define um marcador inicial para utilizar o método limitar()
    public void pular( int marcador ) {
        this.marcadorInicial = marcador > 0 ? marcador : 0;
    }

    public int getMarcadorInicial() {
        return this.marcadorInicial;
    }

    /*
     * recebe um inteiro e retorna os n primeiros
     * itens (n é o parâmetro deste método) a partir do marcador inicial definido no método acima, exemplo
     */
    public ArrayList<Item> limitar( int qtdDeItensLimitada ) {
        ArrayList<Item> itensLimitados = new ArrayList<Item>( qtdDeItensLimitada );
        int fim = this.marcadorInicial + qtdDeItensLimitada;
        for( int i = marcadorInicial; i < fim && i < inventario.getItens().size(); i++ ) {
            itensLimitados.add( inventario.getItens().get( i ) );
        }
        return itensLimitados;
    }

}
