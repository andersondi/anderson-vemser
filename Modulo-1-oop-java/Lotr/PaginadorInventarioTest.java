
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void testaPularAPartirPosicao0() {
        Inventario inventario = new Inventario(10);
        PaginadorInventario paginadorInventario = new PaginadorInventario( inventario );

        paginadorInventario.pular(0);

        assertEquals(0, paginadorInventario.getMarcadorInicial() );
    }

    @Test
    public void testaPularAPartirPosicao1() {
        Inventario inventario = new Inventario(10);
        PaginadorInventario paginadorInventario = new PaginadorInventario( inventario );

        paginadorInventario.pular(1);

        assertEquals(1, paginadorInventario.getMarcadorInicial() );
    }

    @Test
    public void testaLimitar2ItensAPartirDaPosicao2() {
        Inventario inventario = new Inventario(10);
        PaginadorInventario paginadorInventario = new PaginadorInventario( inventario );

        Item arco = new Item(1, "Arco");
        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(4, "Pocao de mana");
        Item pocaoDeCura = new Item(5, "Pocao de cura");

        inventario.adicionarItem( arco );
        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );

        paginadorInventario.pular(0);

        assertEquals(arco, paginadorInventario.limitar(2).get(0) );
        assertEquals(pocaoAntiVeneno, paginadorInventario.limitar(2).get(1) );
    }

    @Test
    public void testaLimitar3ItensAPartirDaPosicao1() {
        Inventario inventario = new Inventario(10);
        PaginadorInventario paginadorInventario = new PaginadorInventario( inventario );

        Item arco = new Item(1, "Arco");
        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(4, "Pocao de mana");
        Item pocaoDeCura = new Item(5, "Pocao de cura");

        inventario.adicionarItem( arco );
        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );

        paginadorInventario.pular(1);

        assertEquals(pocaoAntiVeneno, paginadorInventario.limitar(3).get(0) );
        assertEquals(pocaoDeMana, paginadorInventario.limitar(3).get(1) );
        assertEquals(pocaoDeCura, paginadorInventario.limitar(3).get(2) );

    }
}
