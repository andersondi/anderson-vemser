
public class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
    protected double danoSofrido;

    {
        experiencia = 0;
        inventario = new Inventario(10);
        status = Status.RECEM_CRIADO;
        qtdExperienciaPorAtaque = 1;
        danoSofrido = 0.0;
    }

    public Personagem ( String nome ){
        this.nome = nome;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public Status getStatus(){
        return this.status;
    }

    public void setStatus( Status status ) {
        this.status = status;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    public double getVida() {
        return this.vida;
    }

    public void aumentarXp() {
        experiencia += qtdExperienciaPorAtaque;
    }

    public boolean podeSofrerDano() {
        return this.vida > 0;
    }

    public boolean verificaSeEstaVivo() {
        if( this.getStatus() != Status.MORTO ) {
            return true;
        }
        return false;
    }

    public void ganharItem( Item item ) {
        this.inventario.adicionarItem( item );
    }

    public void perderItem( Item item ) {
        this.inventario.removerItem( item );
    }
    /*
    public double calcularDano(){
        return this.danoSofrido;
    }
    */
    public void sofrerDano() {
        if ( this.podeSofrerDano() && this.verificaSeEstaVivo() ){
            //variavel = comparacao ? verdadeiro : falso;
            this.vida = this.vida >= danoSofrido ?
                this.vida - danoSofrido : 0.0;
            if( this.vida == 0.0 ) {
                this.status = Status.MORTO;
            }else {
                this.status = Status.SOFREU_DANO;
            }
        }
    }
}
