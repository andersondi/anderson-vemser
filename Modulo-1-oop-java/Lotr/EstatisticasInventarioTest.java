
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void testaMedia2Itens(){
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );
        Item arco = new Item(2, "Arco");
        Item espada = new Item(4, "Espada");
        inventario.adicionarItem( arco );
        inventario.adicionarItem( espada );
        assertEquals( 3.0, estatisticaInventario.calcularMedia(), 1e-9 );
    }

    @Test
    public void testaMedia3Itens(){
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );
        Item arco = new Item(1, "Arco");
        Item pocaoDeMana = new Item(3, "Pocao de mana");
        Item pocaoDeCura = new Item(5, "Pocao de cura");

        inventario.adicionarItem( arco );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );
        assertEquals( 3.0, estatisticaInventario.calcularMedia(), 1e-9 );
    }

    @Test
    public void testaMedianaQuantidadePar(){
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );

        Item arco = new Item(1, "Arco");
        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(4, "Pocao de mana");
        Item pocaoDeCura = new Item(5, "Pocao de cura");

        inventario.adicionarItem( arco );
        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );
        assertEquals( 3.0, estatisticaInventario.calcularMediana(), 1e-9 );
    }

    @Test
    public void testaMedianaQuantidadeImpar(){
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );

        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(4, "Pocao de mana");
        Item pocaoDeCura = new Item(5, "Pocao de cura");

        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );

        assertEquals( 4.0, estatisticaInventario.calcularMediana(), 1e-9 );
    }

    @Test
    public void testaQtdItensAcimaDaMediaPara5ItensE2AcimaDaMedia() {
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );
        
        Item arco = new Item(1, "Arco");
        Item espada = new Item(2, "Espada curta");
        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(3, "Pocao de mana");
        Item pocaoDeCura = new Item(4, "Pocao de cura");
        
        inventario.adicionarItem( arco );
        inventario.adicionarItem( espada );
        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );
        
        assertEquals( 2, estatisticaInventario.qtdItensAcimaDaMedia() );
    }
    
    @Test
    public void testaQtdItensAcimaDaMediaPara4ItensNenhumAcimaDaMedia() {
        Inventario inventario = new Inventario(10);
        EstatisticasInventario estatisticaInventario = new EstatisticasInventario( inventario );
        
        Item espada = new Item(2, "Espada curta");
        Item pocaoAntiVeneno = new Item(2, "Pocao Antiveneno");
        Item pocaoDeMana = new Item(2, "Pocao de mana");
        Item pocaoDeCura = new Item(2, "Pocao de cura");
        
        inventario.adicionarItem( espada );
        inventario.adicionarItem( pocaoAntiVeneno );
        inventario.adicionarItem( pocaoDeMana );
        inventario.adicionarItem( pocaoDeCura );
        
        assertEquals( 0, estatisticaInventario.qtdItensAcimaDaMedia() );
    }
}
