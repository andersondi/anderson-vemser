/* Classe dwarf */
public class Dwarf extends Personagem{
/*
    private boolean equipado;
    {
        equipado = false;
    }
*/
    //CONSTRUTOR da classe Dwarf
    public Dwarf( String nome ) {
        super( nome );
        this.vida = 110.0;
        this.inventario.adicionarItem( new Item(1, "Escudo") );
        this.danoSofrido = 10.0;
    }

    public void equiparEscudo(){
        this.danoSofrido = 5.0;
        //this.equipado = true;
    }
    /*
    public double calcularDano(){
        return this.equipado ? 5.0 : 10.0;
    }
    /*
    public void sofrerDano() {
        if ( super.podeSofrerDano() && super.verificaSeEstaVivo() ){
            //variavel = comparacao ? verdadeiro : falso;
            this.vida = this.vida >= calcularDano() ?
                this.vida - calcularDano() : 0.0;
            if( this.vida == 0.0 ) {
                this.status = Status.MORTO;
            }else{
                this.status = Status.SOFREU_DANO;
            }
        }
    }
    */
}
