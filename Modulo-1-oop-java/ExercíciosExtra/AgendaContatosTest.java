
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void adicionarUmContato() {
        AgendaContatos agenda = new AgendaContatos();
        Contato anderson = new Contato("Anderson","12345");
        agenda.adicionar(anderson);
        
        assertEquals(anderson, agenda.consultar(anderson));
    }
}
