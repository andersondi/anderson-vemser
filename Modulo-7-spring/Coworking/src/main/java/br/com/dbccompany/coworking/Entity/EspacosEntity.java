package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "ESPACOS")
public class EspacosEntity {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ" )
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_ESPACOS" )
    private Long id;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosXPacotesEntity> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco" )
    private List<ContratacoesEntity> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "espaco")
    private List<SaldosXClientesEntity> saldoClientes = new ArrayList<>();

    @Column( nullable = false, unique = true )
    private String nome;

    @Column( name = "QUANT_PESSOAS", nullable = false )
    private Long quantidadePessoas;

    @Column( nullable = false )
    private Long valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<EspacosXPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosXPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacoesEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacoesEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<SaldosXClientesEntity> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldosXClientesEntity> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Long quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
