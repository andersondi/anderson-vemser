package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContatosEntity;
import br.com.dbccompany.coworking.Service.TipoContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/tipocontato")
public class TipoContatosController {

    @Autowired
    TipoContatosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoContatosEntity> todosTipoContatos(){
        return service.todosTipoContato();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContatosEntity novoTipoContato( @RequestBody TipoContatosEntity tipoContato ){
        return service.salvar( tipoContato );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContatosEntity editarTipoContato( @PathVariable Long id, @RequestBody TipoContatosEntity tipoContato ){
        return service.editar( tipoContato, id );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public TipoContatosEntity buscarPorIdTipoContato(@PathVariable Long id ){
        return service.tipoContatoEspecifico( id );
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public TipoContatosEntity buscarPorNome(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Long id ) {
        service.delete( id );
        return true;
    }

}
