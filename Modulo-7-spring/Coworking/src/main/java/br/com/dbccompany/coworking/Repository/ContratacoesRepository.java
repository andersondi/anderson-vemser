package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacoesRepository extends CrudRepository<ContratacoesEntity, Long> {

  List<ContratacoesEntity> findAll();

}