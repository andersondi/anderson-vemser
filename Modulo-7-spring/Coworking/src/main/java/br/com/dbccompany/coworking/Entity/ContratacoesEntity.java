package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CONTRATACOES")
public class ContratacoesEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTRATACOES_SEQ", sequenceName = "CONTRATACOES_SEQ" )
    @GeneratedValue( generator = "CONTRATACOES_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID", nullable = false )
    private Long id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESPACO", nullable = false)
    private EspacosEntity espaco;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private ClientesEntity cliente;

    @OneToMany( mappedBy = "contratacao" )
    private List<PagamentosEntity> pagamentos = new ArrayList<>();

    @Enumerated( EnumType.STRING )
    @Column( name = "TIPO_CONTRATACAO", nullable = false )
    private TipoContratacao tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = true )
    private Double desconto;

    @Column( nullable = false)
    private Integer prazo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
