package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class SaldosXClientesEntity {
    @EmbeddedId
    private IdSaldoClientesEntity id;

    @ManyToOne
    @MapsId("ID_CLIENTE")
    @JoinColumn( name = "ID_CLIENTE", nullable = false )
    private ClientesEntity cliente;

    @ManyToOne
    @MapsId("ID_ESPACO")
    @JoinColumn( name = "ID_ESPACO", nullable = false )
    private EspacosEntity espaco;

    @Enumerated( EnumType.STRING )
    @Column( name = "TIPO_CONTRATACAO", nullable = false )
    private TipoContratacao tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private Date vencimento;

    public IdSaldoClientesEntity getId() {
        return id;
    }

    public void setId(IdSaldoClientesEntity id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
