package br.com.dbccompany.coworking.Entity;

public enum TipoContratacao {
    MES, SEMANA, DIARIA, TURNO, HORA, MINUTO
}