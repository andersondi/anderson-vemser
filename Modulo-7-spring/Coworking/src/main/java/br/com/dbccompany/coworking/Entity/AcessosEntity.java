package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "ACESSOS")
public class AcessosEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSO", nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @MapsId("ID_CLIENTE")
    @JoinColumn(name = "ID_CLIENTES_SALDO_CLIENTE")
    private ClientesEntity cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @MapsId("ID_ESPACO")
    @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE")
    private EspacosEntity espaco;

    private Boolean isEntrada;

    private Date data;

    private Boolean isExcecao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}