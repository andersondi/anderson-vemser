package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ContratacoesEntity;
import br.com.dbccompany.coworking.Repository.ContratacoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacoesService {

    @Autowired
    private ContratacoesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContratacoesEntity salvar(ContratacoesEntity contratacoes) {
        return repository.save(contratacoes);
    }

    @Transactional(rollbackFor = Exception.class)
    public ContratacoesEntity editar(ContratacoesEntity contratacoes, Long id) {
        contratacoes.setId(id);
        return repository.save(contratacoes);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<ContratacoesEntity> buscarTodosContratacoes() {
        return (List<ContratacoesEntity>) repository.findAll();
    }

    public ContratacoesEntity contratacoesEspecifico(Long id) {
        Optional<ContratacoesEntity> contratacoes = repository.findById(id);
        return contratacoes.get();
    }
}
  