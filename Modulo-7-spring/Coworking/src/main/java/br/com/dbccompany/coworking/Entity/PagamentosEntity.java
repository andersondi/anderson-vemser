package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAGAMENTOS" )
public class PagamentosEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ" )
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( nullable = false )
    private Long id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CLIENTES_X_PACOTES")
    private ClientesXPacotesEntity clientesPacotes;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CONTRATACAO")
    private ContratacoesEntity contratacao;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_PAGAMENTO", nullable = false )
    private TipoPagamento tipoPagamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientesXPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesXPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContratacoesEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacoesEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
