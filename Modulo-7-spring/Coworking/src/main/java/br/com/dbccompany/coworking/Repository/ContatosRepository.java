package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatosRepository extends CrudRepository<ContatosEntity, Long> {

  List<ContatosEntity> findAll();

}