package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesEntity;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity salvar(ClientesEntity cliente) {
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity editar(ClientesEntity cliente, Long id) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<ClientesEntity> buscarTodosClientes() {
        return (List<ClientesEntity>) repository.findAll();
    }

    public ClientesEntity clienteEspecifico(Long id) {
        Optional<ClientesEntity> cliente = repository.findById(id);
        return cliente.get();
    }
}
