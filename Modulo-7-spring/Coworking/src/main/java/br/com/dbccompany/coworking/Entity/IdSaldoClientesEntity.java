package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class IdSaldoClientesEntity implements Serializable {

    @Column( name = "ID_CLIENTE" )
    private Long idCliente;

    @Column( name = "ID_ESPACO" )
    private Long idEspaco;

    public void IdSaldoClientes() {
    }

    public void IdSaldoClientes( Long idCliente, Long idEspaco ) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente( Long idCliente ) {
        this.idCliente = idCliente;
    }

    public Long getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco( Long idEspaco ) {
        this.idEspaco = idEspaco;
    }
}