package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "ESPACOS_X_PACOTES")
public class EspacosXPacotesEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_X_PACOTES_SEQ", sequenceName = "ESPACOS_X_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS_X_PACOTES")
    private Long id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESPACO", nullable = false )
    private EspacosEntity espaco;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PACOTE", nullable = false )
    private PacotesEntity pacote;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO" )
    private TipoContratacao tipoContratacao;

    @Column
    private Integer quantidade;

    @Column( name = "PRAZO" )
    private Integer prazo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
