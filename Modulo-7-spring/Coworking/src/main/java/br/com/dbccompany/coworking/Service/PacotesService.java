package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.PacotesEntity;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity salvar(PacotesEntity pacotes) {
        return repository.save(pacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity editar(PacotesEntity pacotes, Long id) {
        pacotes.setId(id);
        return repository.save(pacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<PacotesEntity> buscarTodosPacotes() {
        return (List<PacotesEntity>) repository.findAll();
    }

    public PacotesEntity pacotesEspecifico(Long id) {
        Optional<PacotesEntity> pacotes = repository.findById(id);
        return pacotes.get();
    }
}
  