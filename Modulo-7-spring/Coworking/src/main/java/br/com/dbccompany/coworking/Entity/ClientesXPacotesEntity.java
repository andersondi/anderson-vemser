package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CLIENTES_X_PACOTES")
public class ClientesXPacotesEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTES_X_PACOTES")
    private Long id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private ClientesEntity cliente;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PACOTE", nullable = false )
    private PacotesEntity pacote;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<PagamentosEntity> pagamentos = new ArrayList<>();

    @Column( nullable = false )
    private Long quantidade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }
}
