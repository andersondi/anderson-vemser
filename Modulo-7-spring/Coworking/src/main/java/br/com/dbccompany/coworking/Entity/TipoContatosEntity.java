package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "TIPO_CONTATOS")
public class TipoContatosEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "TIPO_CONTATOS_SEQ", sequenceName = "TIPO_CONTATOS_SEQ")
    @GeneratedValue( generator = "TIPO_CONTATOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_TIPO_CONTATOS" )
    private Long id;

    private String nome;

    public Long getId() {

        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {

        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
}

