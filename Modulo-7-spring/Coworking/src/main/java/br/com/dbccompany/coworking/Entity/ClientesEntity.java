package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table( name = "CLIENTES")
public class ClientesEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_CLIENTES" )
    private Long id;

    @OneToMany( mappedBy = "cliente" )
    private List<ContatosEntity> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<ContratacoesEntity> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<ClientesXPacotesEntity> clientesXPacotes = new ArrayList<>();

//    @OneToMany( mappedBy = "cliente")
//    private List<SaldosClientesEntity> saldoClientes = new ArrayList<>();

    @Column( nullable = false )
    private Long valor;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true, length = 14)
    private String cpf;
    
    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ContatosEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatosEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacoesEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacoesEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<ClientesXPacotesEntity> getClientesXPacotes() {
        return clientesXPacotes;
    }

    public void setClientesXPacotes(List<ClientesXPacotesEntity> clientesXPacotes) {
        this.clientesXPacotes = clientesXPacotes;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}

