package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosEntity;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity salvar(EspacosEntity espacos) {
        return repository.save(espacos);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity editar(EspacosEntity espacos, Long id) {
        espacos.setId(id);
        return repository.save(espacos);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<EspacosEntity> buscarTodosEspacos() {
        return (List<EspacosEntity>) repository.findAll();
    }

    public EspacosEntity espacosEspecifico(Long id) {
        Optional<EspacosEntity> espacos = repository.findById(id);
        return espacos.get();
    }
}
  