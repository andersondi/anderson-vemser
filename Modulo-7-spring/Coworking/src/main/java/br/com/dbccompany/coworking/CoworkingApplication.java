package br.com.dbccompany.coworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(br.com.dbccompany.coworking.CoworkingApplication.class, args);
	}

}
