package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATOS")
public class ContatosEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTATOS_SEQ", sequenceName = "CONTATOS_SEQ" )
    @GeneratedValue( generator = "CONTATOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID_CONTATOS" )
    private Long id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_TIPO_CONTATOS", nullable = false )
    private TipoContatosEntity tipoContato;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTES", nullable = false )
    private ClientesEntity cliente;

    @Column
    private Long valor;

    public Long getId() {

        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Long getValor() {

        return valor;
    }
    public void setNome(Long valor) {
        this.valor = valor;
    }

    public TipoContatosEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatosEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}

