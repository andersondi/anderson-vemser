package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatosRepository extends CrudRepository<TipoContatosEntity, Long> {

  TipoContatosEntity findByNome( String nome );
  List<TipoContatosEntity> findAll();

}