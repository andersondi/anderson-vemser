package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity salvar(PagamentosEntity pagamentos) {
        return repository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity editar(PagamentosEntity pagamentos, Long id) {
        pagamentos.setId(id);
        return repository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<PagamentosEntity> buscarTodosPagamentos() {
        return (List<PagamentosEntity>) repository.findAll();
    }

    public PagamentosEntity pagamentosEspecifico(Long id) {
        Optional<PagamentosEntity> pagamentos = repository.findById(id);
        return pagamentos.get();
    }
}
  