package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public AcessosEntity salvar(AcessosEntity acessos) {
        return repository.save(acessos);
    }

    @Transactional(rollbackFor = Exception.class)
    public AcessosEntity editar(AcessosEntity acessos, Long id) {
        acessos.setId(id);
        return repository.save(acessos);
    }

    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<AcessosEntity> buscarTodosAcessos() {
        return (List<AcessosEntity>) repository.findAll();
    }

    public AcessosEntity acessosEspecifico(Long id) {
        Optional<AcessosEntity> acessos = repository.findById(id);
        return acessos.get();
    }
}
  