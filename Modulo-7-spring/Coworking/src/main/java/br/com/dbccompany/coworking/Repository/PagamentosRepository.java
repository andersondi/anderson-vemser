package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Long> {

  List<PagamentosEntity> findAll();

}