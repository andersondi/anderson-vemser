package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ContatosEntity;
import br.com.dbccompany.coworking.Repository.ContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatosService {

    @Autowired
    private ContatosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ContatosEntity salvar(ContatosEntity contatos) {
        return repository.save(contatos);
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatosEntity editar(ContatosEntity contatos, Long id) {
        contatos.setId(id);
        return repository.save(contatos);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Long id) {
        repository.deleteById(id);
    }

    public List<ContatosEntity> buscarTodosContatos() {
        return (List<ContatosEntity>) repository.findAll();
    }

    public ContatosEntity contatosEspecifico(Long id) {
        Optional<ContatosEntity> contatos = repository.findById(id);
        return contatos.get();
    }
}
  