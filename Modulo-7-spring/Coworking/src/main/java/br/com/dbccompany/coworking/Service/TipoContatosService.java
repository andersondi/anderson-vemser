package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TipoContatosEntity;
import br.com.dbccompany.coworking.Repository.TipoContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatosService {

    @Autowired
    private TipoContatosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContatosEntity salvar(TipoContatosEntity tipoContatos) {
        return repository.save(tipoContatos);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContatosEntity editar(TipoContatosEntity tipoContatos, Long id) {
        tipoContatos.setId(id);
        return repository.save(tipoContatos);
    }

    public List<TipoContatosEntity> todosTipoContato(){
        return repository.findAll();
    }

    public TipoContatosEntity tipoContatoEspecifico( Long id ){
        Optional<TipoContatosEntity> tipoContato = repository.findById( id );
        return tipoContato.get();
    }

    public TipoContatosEntity findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public void delete( Long id ){
        repository.deleteById( id );
    }
}
  