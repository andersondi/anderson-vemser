package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "PACOTES")
public class PacotesEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_PACOTES" )
    private Long id;

    @OneToMany(mappedBy = "pacote")
    private List<EspacosXPacotesEntity> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote" )
    private List<ClientesXPacotesEntity> clientesPacotes = new ArrayList<>();

    @Column( nullable = false )
    private Long valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<EspacosXPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosXPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesXPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesXPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}

