package br.com.dbccompany.bancodigital.Security;

import br.com.dbccompany.bancodigital.Security.TokenAuthenticationService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthenticationFilter extends GenericFilterBean {
    @Override
public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletResponse res = (HttpServletResponse) response;
    res.setHeader("Acess-Control-Allow-Origin","*");
    res.setHeader("Acess-Control-Allow-Methods","POST, PUT, GET DELETE");
    res.setHeader("Acess-Control-Allow-Age","3600");
    res.setHeader("Acess-Control-Allow-Origin","Authorization, x-xsrf-token, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With" +
            "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    Authentication authentication = TokenAuthenticationService
            .getAuthentication((HttpServletRequest) request);
    SecurityContextHolder.getContext().setAuthentication(authentication);
    chain.doFilter(request, res);
    }
}