const fs = require('fs');
const state = require("./state.js")

const repositoryString = "Acessos, Clientes, Contatos, Contratacoes, Espacos, Pacotes, Pagamentos, TipoContatos"

const convertedList = repositoryString.split(", ")

const makeRepositoryFiles = convertedList.forEach(item => {

  const contentOfEntity = `${item}`

  const contentOfRepository = `${item}`

  const contentOfService = `${item}`

  const contentOfController = `${item}`

  const entity = './Entity';
  const repository = './Repository';
  const service = './Service';
  const controller = './Controller';

  const dirs = [entity, repository, service, controller];

  dirs.forEach(dir => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    console.log(`Diretorio ${dir} criado`);
  }

  const path = "/home/vemser/Área de Trabalho/anderson-vemser/"

  state.save(contentOfEntity, `${path}Entity/`, `${item}Entity`, ".java")
  state.save(contentOfRepository, `${path}Repository/`, `${item}Repository`, ".java")
  state.save(contentOfService, `${path}Service/`, `${item}Service`, ".java")
  state.save(contentOfController, `${path}Controller/`, `${item}Controller`, ".java")

})
