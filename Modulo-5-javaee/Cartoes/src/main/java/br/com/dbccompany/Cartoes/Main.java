package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.LojaXCredenciadorEntity;


public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("VemSerStore");
			
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Cliente-teste");
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("Credenciador-teste");
			
			List<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("VemSer");
			bandeira.setTaxa(0.1);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Emissor-teste");
			emissor.setTaxa(0.1);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setBandeira(bandeira);
			cartao.setChip(true);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
			cartao.setVencimento("05/22");
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setCartao(cartao);
			lancamento.setData_compra("28/05/2020");
			lancamento.setDescricao("debito");
			lancamento.setValor(100.0);
			
			LojaXCredenciadorEntity lojaXCredenciador = new LojaXCredenciadorEntity();
			lojaXCredenciador.setCredenciador(credenciadores);
			lojaXCredenciador.setLoja(lojas);
			lojaXCredenciador.setTaxa(0.05);
			
			session.save(loja);
			session.save(credenciador);
			session.save(bandeira);
			session.save(emissor);
			session.save(cliente);
			session.save(cartao);
			session.save(lancamento);
			session.save(lojaXCredenciador);
			
			System.out.println("Valor para Credenciador: " + lancamento.getValor() * lojaXCredenciador.getTaxa());
			System.out.println("Valor para Bandeira: " + lancamento.getValor() * bandeira.getTaxa());
			System.out.println("Valor para Emissor: " + lancamento.getValor() * emissor.getTaxa());
			
			transaction.commit();
		}catch(Exception e){
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	
	}
}
	